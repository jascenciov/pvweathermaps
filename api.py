import pandas as pd
import numpy as np
import os
from statistics import mode 
import matplotlib.pyplot as plt
import seaborn as sns


def get_closest(i_lat,i_lon,df,namecol_lat ='Lat',namecol_lon='Lon'):
    i_min_value= np.nanargmin( np.abs(i_lat - df[namecol_lat].values) + np.abs(i_lon - df[namecol_lon].values))
    ix = df.index.values[i_min_value]
    return pd.DataFrame(df.iloc[ix,:]).T
    
def getting_DF_fromlists(list_Longitude,list_Latitude,list_VALUES):
    df = pd.DataFrame.from_dict(np.array([list_Longitude,list_Latitude,list_VALUES]).T)
    df.columns = ['X_value','Y_value','Z_value']
    df['Z_value'] = pd.to_numeric(df['Z_value'])
    
    df = pd.pivot_table(df, values='Z_value', index=['Y_value'],columns=['X_value'])
    
    return df

def convert_Mlist_to_DF(MList):
    dict0 = MList.values
    dict1 = MList.index.get_level_values(0)
    dict2 = MList.index.get_level_values(1)
    
    df = getting_DF_fromlists(dict2,dict1,dict0)
    
    df = df.reindex(index=df.index[::-1])  

    return df
    
def get_KGPV_location(latitude, longitude):
    print('Get KGPV for a specific location')
    print(latitude)
    print(longitude)
    return latitude,longitude