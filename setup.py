import setuptools

setuptools.setup(
     
    name="pvweathermaps",
    version="0.1.3",
    author="Julián Ascencio-Vásquez",
    author_email="julian.ascencio@3e.eu",
    description="Easy access to the Photovoltaic Weather Maps",
    url="https://gitlab.com/jascenciov/pvweathermaps",
    packages=setuptools.find_packages(),
    classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
    ],
 )