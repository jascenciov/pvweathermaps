# Changelog

### pvmaps v0.1.2 (Sep 06, 2020)
Fix bug package installation

### pvmaps v0.1.1 (Aug 30, 2020)
initial version of the package (stub)
