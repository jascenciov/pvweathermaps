"""
Unit tests for the PV weathermaps library
"""

import pvweathermaps


class TestPVWeatherMaps:

    def test_get_KGPV_location(self):
        assert 2 == pvweathermaps.get_KGPV_location(2, 2)[0]

