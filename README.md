# PV Weather Maps - LPVO University of Ljubljana
## J.Ascencio-Vásquez, K.Brecl and M.Topič

Easy access to the weather classifications for PV applications.

- Koppen-Geiger-Photovoltaic (KGPV) climate classification

"Methodology of Köppen-Geiger-Photovoltaic climate classification and implications to worldwide mapping of PV system performance"
Open-access Solar Energy: https://www.sciencedirect.com/science/article/pii/S0038092X19308527

- Modelled PV Degradation Mechanisms and Degradation Rates

"Global Climate Data Processing and Mapping of Degradation Mechanisms and Degradation Rates of PV Modules"
Open-access Energies MDPI: https://www.mdpi.com/1996-1073/12/24/4749

- Weather Risks: Soiling, Snow Load and Thunderstorms

"Geographical approach for weather risk identification and PV performance assessment"
To be presented at EU PVSEC 2020 / Online / Student Award Finalist

Packages needed:
https://proj.org/install.html

Contact:
Julián Ascencio-Vásquez
julian.ascencio.91@gmail.com